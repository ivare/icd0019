package junit;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Code {

    public static boolean isSpecial(Integer number) { //TEE LÕPUNI!

        return number%11==0 || number%11==1;
    }

    public static Integer longestStreak(String input) {
        if (input.length()==0){
            return 0;
        }
        String[] characters=input.split("");
        String lastSymbol=characters[0];
        int streakLength=0;
        int longestStreak=0;
        for (String currentSymbol : characters) {
            if (currentSymbol.equals(lastSymbol)){
                streakLength++;
            } else {
                streakLength=1;
            }
            if (longestStreak<streakLength) {
                longestStreak = streakLength;
            }
            lastSymbol=currentSymbol;
        }
        return longestStreak;

    }

        /* Iseseisev lahendus
        if (input.length() == 0) {
            return 0;
        }
        String[] inputToArray2 = new String[Array.getLength(input.split(""))];
        String[] inputToArray = input.split("");
        String mostPopLetter = inputToArray[1];

        Integer topLetterNr = 0;
        int charAtIndex = 0;
        int countOfSymbols = 0;
        for (int i = 0; i < inputToArray.length-1; i++) {
            if (i < (inputToArray.length) - 1) {
                if (inputToArray[i].equals(inputToArray[i + 1])) {
                    countOfSymbols++;
                    topLetterNr = countOfSymbols;
                } else {
                    countOfSymbols = 1;
                }


            }

        }
        return topLetterNr;
    } */

    public static Integer[] removeDuplicates(Integer[] input) {
        Arrays.sort(input);
        Integer[] duplicateArray = new Integer[input.length];
        int j = 0;
        duplicateArray[0]=input[0];
        for (int i = 0; i < input.length - 1; i++) {
            if (input[i] != input[i + 1]) {
                duplicateArray[j++] = input[i]; //proovi siin ka j++ hiljem panna
            }

        }
        duplicateArray[j++]=input[input.length-1];
        //Lets remove null's from Array
        int counter=0;
        for (int i = 0; i < duplicateArray.length; i++) {


            if (duplicateArray[i] != null) {
                counter++;
            }
        }
        //Lets create new array with proper length
        Integer[] returnedArray=new Integer[counter];
        //Lets add values into it.
        for (int i = 0; i < returnedArray.length; i++) {
            returnedArray[i]=duplicateArray[i];

            }



        return returnedArray;
    }



    public static Integer sumIgnoringDuplicates(Integer[] input) {
        Arrays.sort(input);
        Integer[] duplicateArray = new Integer[input.length];
        int j = 0;
        duplicateArray[0]=input[0];
        for (int i = 0; i < input.length - 1; i++) {
            if (input[i] != input[i + 1]) {
                duplicateArray[j++] = input[i]; //proovi siin ka j++ hiljem panna
            }

        }
        duplicateArray[j++]=input[input.length-1];
        //Lets remove null's from Array
        int counter=0;
        for (int i = 0; i < duplicateArray.length; i++) {


            if (duplicateArray[i] != null) {
                counter++;
            }
        }
        //Lets create new array with proper length
        Integer[] returnedArray=new Integer[counter];
        //Lets add values into it.
        for (int i = 0; i < returnedArray.length; i++) {
            returnedArray[i]=duplicateArray[i];

        }
        Integer sumWithoutDuplicates=0;
        for (int i = 0; i < returnedArray.length; i++) {
            sumWithoutDuplicates+=returnedArray[i];

        }

        return sumWithoutDuplicates;

    }

}
