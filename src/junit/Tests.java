package junit;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void equalityExamples() {
        String a = "a";
        Integer x2 = 1;
        Integer y2 = 1;
        Integer x = 128;
        Integer y = 128;
        assertTrue(1==1);
        assertTrue(y2==x2);
        assertTrue("abc"=="a"+"bc");
        assertTrue("abc".equals(a+"bc"));
        assertFalse(1==2);
        assertFalse(x==y);
        assertTrue(x.equals(y));
    }

    @Test
    public void assertThatAndAssertEqualsExample() {
        //2. Kasutage konstruktsioone
        assertEquals(1+2, 3);
        assertThat(1+2, is(3));
        assertThat(1+2, is(equalTo(3)));
        assertThat(3, is(not(2)));

        //2. Nüüd massiividega
        assertEquals(new Integer[] {1, 2, 3}, new Integer[] {1, 2, 3});
        assertThat(new Integer[] {1, 2, 3}, is(new Integer[] {1, 2, 3}));
        assertThat(new Integer[] {1, 2, 3}, is(equalTo(new Integer[] {1, 2, 3})));
        assertThat(new Integer[] {1, 2, 3}, is(not(new Integer[] {1, 2, 2})));

    }

    @Test
    public void findsSpecialNumbers() {
        assertFalse(Code.isSpecial(2));

        assertTrue(Code.isSpecial(11));

        assertTrue(Code.isSpecial(12));

        assertFalse(Code.isSpecial(13));

        assertTrue(Code.isSpecial(33));

    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak("abbb"), is(3));


        assertThat(Code.longestStreak(""), is(0));

        //assertThat(Code.longestStreak("a"), is(1));

        assertThat(Code.longestStreak("abccc"), is(3));



        assertThat(Code.longestStreak("abbcccaaaad"), is(4));

    }

    @Test
    public void removesDuplicates() {

        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 2)), is(arrayOf(1, 2)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 2)), is(3));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private Integer[] arrayOf(Integer... numbers) {
        return numbers;
    }

}
