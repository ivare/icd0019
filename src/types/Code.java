package types;
//Kirjutage faili Code.java average() meetodi sisu. Meetod võtab sisendina
//   täisarvude masiivi ja tagastab elementide keskmise väärtuse.
public class Code {

    public static void main(String[] args) {

        Integer[] numbers = new Integer[] {1, 3, -2, 9};
        String s="a2b9";

        System.out.println(sum(numbers)); // 11
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        System.out.println(squareDigits(s));
    }

    public static Integer sum(Integer[] numbers) {
        Integer x=0;
        for (Integer number : numbers) {
            x = x + number;
        }
        return x;
    }

    public static Double average(Integer[] numbers) {
        Double x=0.0;
        Double y=0.0;
        for (Integer number : numbers) {
            x+=number;
            y++;
        }
        return x/y;
    }

    public static Integer minimumElement(Integer[] integers) {

        //Integer x=integers[0];
        Integer x=null;
        for (Integer integer : integers) {
            if (x==null){
                x=integer;
            }
            if (integer < x) {
                x = integer;
            }
        }
        return x;
    }

    public static String asString(Integer[] elements) { //VÕTA VIIMANE KOMA ÄRA!
        String addThis="";
        Integer count=0;
        for (Integer element : elements) {
            count++;
            addThis+=element;
            if (elements.length!=count) {
                addThis+=", ";

            }
        }

        return addThis;
    }

    public static String squareDigits(String s) {
        Integer charCount=s.length();
        String newString="";
        Integer newNumber;
        for (int i = 0; i < charCount; i++) {
            char charAtIndex=s.charAt(i);
            if (Character.isDigit(charAtIndex)){
                String stringToNr=""+charAtIndex;
                newNumber=Integer.parseInt(stringToNr);
                newNumber=newNumber*newNumber;
                newString+=""+newNumber;
            } else {
                newString+=""+charAtIndex;
            }

        }

        return newString;
    }


}
