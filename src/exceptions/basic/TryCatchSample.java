package exceptions.basic;

public class TryCatchSample {
    public String readDataFrom(Resource resource) {

        String newData= null;
        try {
            resource.open();
            newData = resource.read();
        } catch (Exception e) {
            return "someDefaultValue";
        } finally {
            resource.close();
        }


        //throw new RuntimeException("not implemented yet");
        return newData;
    }
}
