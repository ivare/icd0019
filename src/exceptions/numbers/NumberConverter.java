package exceptions.numbers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

public class NumberConverter {
    String lang;
    Properties properties;


    public NumberConverter(String lang) {


        this.lang = lang;
        this.properties=loadProperties(lang);


    }
    private Properties loadProperties(String lang){
        String filePath = String.format(
                "src/exceptions/numbers/numbers_%s.properties", lang);
        FileInputStream is = null;
        Properties properties = new Properties();

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, Charset.forName("UTF-8"));

            properties.load(reader);

            is.close();

        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(e.getMessage());
        } catch (IOException e) {
            throw new MissingLanguageFileException(e.getMessage());
        }
        return properties;
    }

    public String numberInWords(Integer number) {
        if ((properties.isEmpty())) {

            throw new MissingTranslationException();
        }

        //throw new RuntimeException("not implemented yet");
        //MÕLEMA KOHTA KÄIV
        if (properties.containsKey(String.valueOf(number))) {
            return properties.getProperty(String.valueOf(number));
        }
        //EESTI KEELES
        if (lang.equals("et") && number > 10 && number < 20) {
            int tessa = number % 10;
            String tessaProps = properties.getProperty(String.valueOf(tessa));
            String teenProps = properties.getProperty("teen");
            String kokku = tessaProps + teenProps;
            return kokku;
        }
        if (lang.equals("et") && number == 20) {
            String kakskümmend = properties.getProperty(String.valueOf(String.valueOf(2))) + properties.getProperty(String.valueOf("tens-suffix"));
            return kakskümmend;
        }
        if (lang.equals("en") && (number == 14 || number == 16 || number == 17 || number == 19)) {
            int tessa = number % 10;
            String inglKKokku = properties.getProperty(String.valueOf(tessa)) + properties.getProperty("teen");
            return inglKKokku;
        }

        return "yolopask";
    }
}
