package exceptions.numbers;

public class MissingLanguageFileException extends RuntimeException {

    public MissingLanguageFileException() {
    }

    public MissingLanguageFileException(String message) {
        super(message);
        System.out.println("hi, this is MissingLanguageFileException");
    }
}
