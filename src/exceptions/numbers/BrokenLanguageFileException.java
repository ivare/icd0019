package exceptions.numbers;

public class BrokenLanguageFileException extends RuntimeException {
    public BrokenLanguageFileException() {
    }

    public BrokenLanguageFileException(String message) {
        super(message);
        System.out.println("well, this is BrokenLanguageFileException");
    }

    public BrokenLanguageFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
