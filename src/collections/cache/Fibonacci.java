package collections.cache;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
    Map<Integer, Integer> map=new HashMap<>();

//    public Integer minuCache(Integer m){
//        if(map.containsKey(m)){
//            return map.get(m);
//            //küsi cache'ist
//        } else {
//            map.put()
//        }
//        map.put()
//        return null;
//    }

    public Integer fib(Integer n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if(map.containsKey(n)){
            return map.get(n);
            //küsi cache'ist
        }
        Integer intN=fib(n - 1) + fib(n - 2);
        map.put(n, intN);
        System.out.println("see on map" + map);


        return intN;
    }

}
