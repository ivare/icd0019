package collections.simulator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Hand {
    boolean hasPair=false;
    boolean dontComeHere=false;

    Map<Card.CardValue, Card.CardSuit> kaartideMap=new HashMap<>();

    public void addCard(Card card) {
        if(kaartideMap.containsKey(card.getValue())&&hasPair==true){
            hasPair=false;
            dontComeHere=true;
        } else {
            if(kaartideMap.containsKey(card.getValue())&&dontComeHere==false){
                hasPair=true;
            }
        }


        kaartideMap.put(card.getValue(), card.getSuit());

    }

    @Override
    public String toString() {
        throw new RuntimeException("not implemented yet");
    }

    public boolean isOnePair() {
        return hasPair;

    }

    public boolean isTwoPairs() {
        throw new RuntimeException("not implemented yet");
    }

    public boolean isTrips() {
        throw new RuntimeException("not implemented yet");
    }

    public boolean isFullHouse() {
        throw new RuntimeException("not implemented yet");
    }

}
