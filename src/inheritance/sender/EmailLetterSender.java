package inheritance.sender;

import java.time.LocalTime;

public class EmailLetterSender extends Yhisosa{

    //eemaldasin siit konstruktori ja currentTime muutuja. Lisasin protected klassi yhisosa

    public EmailLetterSender(LocalTime currentTime) {
        super(currentTime);

    }

    public void sendLetter() {
        // simulate sending the email

        System.out.println("Sending email ...");
        System.out.println("Title: SMSMUUTSIN " + greeting());
        System.out.println("Text: " + contents());
    }



}