package inheritance.sender;

import java.time.LocalTime;

public class SmsLetterSender extends Yhisosa{

    public SmsLetterSender(LocalTime currentTime) {
        super(currentTime);
    }

    public void sendLetter() {
       // simulate sending the sms

        System.out.println("Sending sms ...");
        System.out.println("Title: " + greeting());
        System.out.println("Text: " + contents());
    }



}
