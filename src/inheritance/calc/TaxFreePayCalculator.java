package inheritance.calc;

public class TaxFreePayCalculator extends PayCalculator {
    @Override
    public Double getWeeklyPayAfterTaxes(Integer hoursWorked) {

        Integer straightTime = Math.min(40, hoursWorked);
        Integer overTime = Math.max(0, hoursWorked - straightTime);
        Integer straightPay = straightTime * HOUR_RATE;
        Double overtimePay = overTime * OVERTIME_RATE;
        return straightPay + overtimePay;
    }
}
