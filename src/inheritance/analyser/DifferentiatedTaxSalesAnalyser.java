package inheritance.analyser;

public class DifferentiatedTaxSalesAnalyser extends AbstractAnalyzer{


    public DifferentiatedTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }
    @Override
    public Double getTotalSales() {

        Double suurMaks=0.0;
        Double suurKokku=0.0;
        Double väikeMaks=0.0;
        Double väikeKokku=0.0;

        for (int i = 0; i < minuArray.length; i++) {
            if(minuArray[i].hasReducedRate()){
                väikeMaks=(minuArray[i].getItemsSold()*minuArray[i].getProductPrice())/1.1;
                väikeKokku=väikeKokku+väikeMaks;
            }
            if(!minuArray[i].hasReducedRate()){
                suurMaks=(minuArray[i].getItemsSold()*minuArray[i].getProductPrice())/1.2;
                suurKokku=suurKokku+suurMaks;
            }

        }

        return väikeKokku+suurKokku;
    }

    public Double getTotalSalesByProductId(String id) {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfMostPopularItem() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }


}
