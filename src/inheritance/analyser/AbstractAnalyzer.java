package inheritance.analyser;

public abstract class  AbstractAnalyzer {

    SalesRecord[] minuArray;
    public AbstractAnalyzer(SalesRecord[] records) {
        minuArray=records;
    }
    public Double getTotalSales() {
        Integer asi=0;

        Double väljastan=0.0;
        for (int i = 0; i < minuArray.length; i++) {
            asi=minuArray[i].getProductPrice()*minuArray[i].getItemsSold();
            väljastan=väljastan+asi.doubleValue();
        }
        return väljastan;
    }
    public Double getTotalSalesByProductId(String id) {
        Integer asi=0;
        Double väljastan=0.0;
        for (int i = 0; i < minuArray.length; i++) {
            if(id.equals(minuArray[i].getProductId())){
                asi=minuArray[i].getItemsSold()*minuArray[i].getProductPrice();
                väljastan=väljastan+asi.doubleValue();
            }

        }
        return väljastan;

    }


}
