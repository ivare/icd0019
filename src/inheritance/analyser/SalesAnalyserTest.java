package inheritance.analyser;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class SalesAnalyserTest {


    SalesRecord[] records = {
            new SalesRecord("i1", 1, 12),
            new SalesRecord("i2", 2, 24),
            new SalesRecord("i1", 1, 6),
            new SalesRecord("i5", 12, 5),
            new SalesRecord("i5", 12, 5),
            new SalesRecord("i5", 12, 5),
            new SalesRecord("i5", 12, 5),
            new SalesRecord("i4", 24, 2, true)
    };
    String[] minuArr={"adg", "yolo", "sest"};

    @Test
    public void prindiVäljaSales(){
//        SalesRecord yo=new SalesRecord("d", 3, 4);
        FlatTaxSalesAnalyser yo2=new FlatTaxSalesAnalyser(records);
        System.out.println(yo2.minuArray);
        System.out.println(Arrays.toString(records));
//        System.out.println(Arrays.toString(minuArr));
//        System.out.println(yo.getProductId());
        System.out.println(records[3].getProductId());
//        System.out.println(yo2.getTotalSales());
//        System.out.println(Arrays.toString(yo2.tooteHind));
//        System.out.println(Arrays.toString(yo2.tooteKogus));
//        System.out.println(yo2.recordsLocal[3].getProductId());
    }
    @Test
    public void calculatesTotalSalesWithFlatTaxRate() { //arvutab müügitulu arvestades, et müügi infos on hinnad käibemaksuga 20%;
        FlatTaxSalesAnalyser analyser = new FlatTaxSalesAnalyser(records);

        assertThat(analyser.getTotalSales(), is(closeTo(295)));
    }

    @Test
    public void calculatesTotalSalesByProductIdWithFlatTaxRate() {
        FlatTaxSalesAnalyser analyser = new FlatTaxSalesAnalyser(records);

        assertThat(analyser.getTotalSalesByProductId("i1"), is(closeTo(15)));
    }

    @Test
    public void calculatesTotalSalesWithTaxFreeRate() { //arvutab müügitulu arvestades, et müügi infos olevad hinnad ei sisalda käibemaksu.
        TaxFreeSalesAnalyser analyser = new TaxFreeSalesAnalyser(records);

        assertThat(analyser.getTotalSales(), is(closeTo(354)));
    }

    @Test
    public void calculatesTotalSalesByProductIdWithTaxFreeRate() { //KORRAS
        TaxFreeSalesAnalyser analyser = new TaxFreeSalesAnalyser(records);

        assertThat(analyser.getTotalSalesByProductId("i1"), is(closeTo(18)));
    }

    @Test
    public void calculatesTotalSalesWithDifferentiatedTaxRate() { //arvutab müügitulu arvestades, et müügi infos on osad hinnad käibemaksuga 20% ja mõned käibemaksuga 10%;
        DifferentiatedTaxSalesAnalyser analyser = new DifferentiatedTaxSalesAnalyser(records);

        assertThat(analyser.getTotalSales(), is(closeTo(298.6)));
    }

    @Test
    public void allAnalysersHaveCommonAbstractSuperclass() {
        Class<?> s1 = DifferentiatedTaxSalesAnalyser.class.getSuperclass();
        Class<?> s2 = TaxFreeSalesAnalyser.class.getSuperclass();
        Class<?> s3 = FlatTaxSalesAnalyser.class.getSuperclass();

        assertThat(s1.getName(), is(s2.getName()));
        assertThat(s1.getName(), is(s3.getName()));

        assertTrue("Superclass should be abstract",
                Modifier.isAbstract(s1.getModifiers()));
    }

    @Test // -1 point if this test fails
    public void extraFunctionalityWorks() {
        FlatTaxSalesAnalyser analyser = new FlatTaxSalesAnalyser(records);

        assertThat(analyser.getIdOfMostPopularItem(), is("i2"));
        assertThat(analyser.getIdOfItemWithLargestTotalSales(), is("i5"));
    }

    private Matcher<Double> closeTo(double value) {
        double precision = 0.1;

        return Matchers.closeTo(value, precision);
    }

}
