package inheritance.stack;

import java.util.Stack;

public class ExtendedStack extends Stack<Integer> {
    public void pushAll(Integer ... a){
        for (Integer integer : a) {
            push(integer);

        }

    }

    @Override
    public Integer push(Integer item) {
        System.out.println("PUSH() hakkas käima");
        return super.push(item);

    }
}
