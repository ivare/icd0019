package oo.hide;

public class PointSet {
    int lastNumber = 0;
    //Point set = new Point();
    Point[] bigArray;


    public PointSet(Integer initialCapacity) {
        bigArray=new Point[initialCapacity];
    }

    public PointSet() {
        bigArray = new Point[100];

    }

    public void add(Point point) {
        for (int i = 0; i < bigArray.length; i++) {
            if (bigArray[i] != null && bigArray[i].equals(point)) {
                break;
            }
            if (bigArray[i] == null) {
                bigArray[i] = point;
                lastNumber = i;
                break;

            }
        }
    }

    public Integer size() {
        return lastNumber + 1;
    }

    public boolean contains(Point p) {
        for (Point point : bigArray) {
            if (point != null && point.equals(p)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {

        StringBuilder something = new StringBuilder();
        for (Point point : bigArray) {
            if (point != null) {
                something.append(point);
                something.append(", ");

            }

        }
//        if (something.charAt(something.length()-1)==','){
//            something.setCharAt((something.length()-1), ' ');
//    }
//        something.deleteCharAt((something.length()-1));
        something.delete((something.length()-2), (something.length()));

        System.out.println(something.length() + " see pikkus");



        return something.toString();
    }

    public static void main(String[] args) {
        PointSet pointSet = new PointSet();
        System.out.println(pointSet.bigArray.length);
    }
}
