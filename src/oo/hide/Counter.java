package oo.hide;

public class Counter {
    private int start;
    private int step;

    public Counter(Integer start, Integer step) {
        this.start=start;
        this.step=step;
    }

    public Integer nextValue() {
        Integer nextSum=start;
        start+=step;
        return nextSum;
    }
}
