package oo.struct;

import org.junit.Test;

public class Runner {

    @Test
    public void coordinatesAsArrays() {

        Integer[][] trianglePoints = {{1, 1, 0}, {5, 1, 0}, {3, 7, 1}};

        for (Integer[] each : trianglePoints) {
            System.out.println(each[2]);
        }

    }

    @Test
    public void coordinatesAsObjects() { //Miks tehakse Point3D failis väli each Z publikuks?
        Point3D[] trianglePointsNew =
                {new Point3D(1, 1, 0),
                        new Point3D(5, 1, 0),
                        new Point3D(3, 7, 1)};
        for (Point3D each : trianglePointsNew) {
            System.out.println(each.z);
        }

    }


}
